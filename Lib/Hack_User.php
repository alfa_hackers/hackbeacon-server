<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../include/DbConnect.php';
require_once '../include/ValidationHelper.php';
require_once '../include/Config.php';
require_once '../include/PassHash.php';

class HackUser {

    public $user_id;
    public $user_name;
    public $first_name;
    public $last_name;
    public $email;
    public $phone_no;
    public $age;
    public $gender;
    public $role;
    public $password;
    public $registured_on;
    public $last_login;
    public $last_update;
    public $state;

    public static function verify($facebook_user_id) {
        $sql = "SELECT * FROM `api_user_registration_type` WHERE `facebook_user_id` =:facebook_user_id ";
        try {
            $db = DbConnect::connection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("facebook_user_id", $facebook_user_id);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            $db = null;
            return $num_rows > 0;
        } catch (PDOException $e) {
//            echo '{ERROR:{"text":' . $e->getMessage() . '}}';
            return FALSE;
        }
    }

    public static function allUsers($role_id) {

        $sql = "SELECT * FROM `hack_user`  WHERE `role` =:role_id ";

        try {
            $db = DbConnect::connection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("role_id", $role_id);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $stmt->close();
            $db = null;
            return $users;
        } catch (PDOException $e) {
//            echo '{ERROR:{"text":' . $e->getMessage() . '}}';
            return FALSE;
        }
    }

    private static function validate($user) {
        if (!is_a($user, 'HackUser')) {
            $response[ERROR] = true;
            $response[MESSAGE] = 'Invalid data';
            return $response;
        }

        if (!validateEmail($user->email)) {
            $response[ERROR] = true;
            $response[MESSAGE] = 'Email address is not valid';
            return $response;
        }

        if ($user->age <= 0 || $user->age > 99) {
            $response[ERROR] = true;
            $response[MESSAGE] = 'Inappropriate age $user->age';
            return $response;
        }
        
        if ($user->gender <= 0 || $user->gender > 1) {
            $response[ERROR] = true;
            $response[MESSAGE] = 'Inappropriate gender $user->gender';
            return $response;
        }

        if (!empty($user->phone_no) &&
                mb_strlen($user->phone_no, 'utf16') != 10 && !validateMobile($user->phone_no)) {
            $response[ERROR] = true;
            $response[MESSAGE] = 'Inappropriate phone number';
            return $response;
        }
        

        if (!validatePasswordStrength($user->password)) {
            $response[ERROR] = true;
            $response[MESSAGE] = 'Password does not meet the requirements!';
            return $response;
        }

        return TRUE;
    }

    public static function register($user) {
        
        $validationResult = HackUser::validate($user);
        $response = array();
        if ($validationResult != TRUE) {

            return $validationResult;
        }

        if (HackUser::isUserExists($user->email)) {
            $response[ERROR] = true;
            $response[MESSAGE] = "Sorry, this email already existed";
            return $response;
        }

        $stmt = DbConnect::connection()->prepare("INSERT INTO hack_user(user_name, email, phone_no, gender, password) values(?, ?, ?, ?, ?)");
        $password_hash = PassHash::hash($user->password);
        $stmt->bind_param("sssis", $user->user_name, $user->email, $user->phone_no, $user->gender, $password_hash);
        $result = $stmt->execute();
        echoRespnse(200, $stmt->error);
        die();
        $stmt->close();
        
        if ($result) {
            $response[ERROR] = false;
            $response[MESSAGE] = "You are successfully registered";
        } else {
            $response[ERROR] = true;
            $response[MESSAGE] = "Oops! An error occurred while registereing";
        }
        
        return $response;
    }
    
    public static function checkLogin($email, $password) {
        // fetching user by email
        $stmt = DbConnect::connection()->prepare("SELECT password_hash FROM users WHERE email = ?");
 
        $stmt->bind_param("s", $email);
 
        $stmt->execute();
 
        $stmt->bind_result($password_hash);
 
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password
 
            $stmt->fetch();
 
            $stmt->close();
 
            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();
 
            // user not existed with the email
            return FALSE;
        }
    }

    
    public static function getUserByEmail($email) {
        $stmt = DbConnect::connection()->prepare("SELECT `email`, `first_name`, `last_name`, `role`, `status` FROM `hack_user` where `email` = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
    
    public static function getUserByUserName($email) {
        $stmt = DbConnect::connection()->prepare("SELECT `email`, `first_name`, `last_name`, `role`, `status` FROM `hack_user` where `email` = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
    
    private static function isUserExists($email) {
        $stmt = DbConnect::connection()->prepare("SELECT user_id FROM `hack_user` where `email` = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

}
