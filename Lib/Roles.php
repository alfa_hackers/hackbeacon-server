<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../include/DbConnect.php';

class Role {

    function __construct() {
        
    }

    public static function allRoles() {
        $sql = "SELECT * FROM roles";
        
        try {
            $db = DbConnect::connection();
            $stmt = $db->prepare($sql);
            $response = array();
        if ($stmt->execute()){
            $result = $stmt->get_result();
            $stmt->close();
            while ($row = $result->fetch_assoc()) {
                $role = array();
                $role["name"] = $row["name"];
                $role["state"] = $row["state"];
                array_push($response,$role);
            }
            $db = null;
            return $response;
        }
        return FALSE;
        } catch (PDOException $e) {
            return FALSE;
        }
    }
}
