<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require '../vendor/autoload.php';
require_once '../Lib/Roles.php';
require_once '../Lib/Hack_User.php';
require_once '../include/ValidationHelper.php';

$app = new \Slim\Slim();

use RedBeanPHP\R;

R::setup('mysql:host=127.0.0.1:8889;dbname=hackbeacon', 'root', 'root');
R::freeze(true);


function authenticate(\Slim\Route $route) {
    $app = \Slim\Slim::getInstance();
    if (validateUserKey() === false) {
      $app->halt(401);
    }
}


function validateUserKey($uid, $key) {
  return TRUE; // do we need key validation as of now ?
}


$app->get('/roles', 'authenticate', function() use ($app) {
    $roles = R::findAll('roles');
    $app->response()->header('Content-Type', 'application/json');
    $response[DATA] = R::exportAll($roles);
    $response[ERROR] = FALSE;
    $response[MESSAGE] = '';
    echo json_encode($response);
});

$app->get('/role/:id', 'authenticate', function ($id) use ($app) {
    try {
        $role = R::findOne('roles', 'role_id=?', array($id));
        if ($role) {
            $app->response()->header('Content-Type', 'application/json');
            $response[DATA] = R::exportAll($role);
            $response[ERROR] = FALSE;
            $response[MESSAGE] = '';
            echo json_encode($response);
        } else {
            throw new Exception();
        }
    } catch (Exception $e) {
        
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
        $app->response()->header('Content-Type', 'application/json');
        $response[ERROR] = TRUE;
        $response[MESSAGE] = 'Resource not found';
        echo json_encode($response);
    }
});

$app->run();
