<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DbConnect {
    /**
     * Establishing database connection
     * @return database connection handler
     */
    
     static function __connection() {
        $dbhost = "127.0.0.1:8889";
        $dbuser = "root";
        $dbpass = "root";
        $dbname = "hackbeacon";
        $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $dbh;
    }

    public static function connection() {
        
        $dbhost = "127.0.0.1:8889";
        $dbuser = "root";
        $dbpass = "root";
        $dbname = "hackbeacon";
        $conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
 
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
 
        // returing connection resource
        return $conn;
        
    }
}
